#ifndef STRUCTS_HH
#define STRUCTS_HH


#include <string>
#include <sstream>
#include <vector>



enum user_statuses { online, afk, offline };
struct user_struct {
    std::string id = "000000000000";
    std::string name = "unauthenticated";
    std::string pwd_hash = "invalid";
    std::vector<std::string> guilds = {};
    user_statuses status = user_statuses::offline;
};

struct client_struct {
    unsigned int conn_id = 0;    // Always init your vals.
    std::vector<std::string> recv_queue;
    std::string client_ip = "#";
    user_struct *user;
    std::string get_identifier() {
        std::stringstream res;
        res << conn_id << ':' << user->name << '@' << user->id;
        return res.str();
    }
    bool authed = false;
};


#endif // STRUCTS_HH
