#ifndef ENVIRONMENT_HH
#define ENVIRONMENT_HH

extern std::map<std::string, user_struct> users;
extern std::map<int, client_struct> clients;

#endif // ENVIRONMENT_HH
