#include <iostream>
#include <string>
#include <string_view>
#include <map>
#include "structs.hh"
#include "environment.hh"


unsigned int load_users(std::map<std::string, user_struct> &users) {
    // Dummy
    user_struct thisuser;
    thisuser.id = "1234567890";
    thisuser.name = "Testuser";
    thisuser.pwd_hash = "bla";
    users[thisuser.id] = thisuser;
    return 1;
}

user_struct *check_auth(std::string userid, std::string_view password) {
    if (users.find(userid) != users.end()) {
        std::clog << "Loading user struct..." << std::flush;
        user_struct *thisuser = &users[userid];
        std::clog << "Success!" << std::endl;
        std::clog << "Testing read..." << std::endl;
        std::clog << '"' << thisuser->pwd_hash << "\" (works!)" << std::endl;
        // User ID found, check password
        if (thisuser->pwd_hash == password) {
            return thisuser;
        } else {
            return nullptr;
        }
    }
}
