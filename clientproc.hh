#ifndef CLIENTPROC_HH
#define CLIENTPROC_HH


#include <string_view>
#include "structs.hh"

extern void process_message(struct client_struct *thisclient, std::string_view message);


#endif // CLIENTPROC_HH
