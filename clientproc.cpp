#include <iostream>
#include <string>
#include <string_view>
#include <map>
#include <vector>
#include "structs.hh"
#include "environment.hh"
#include "users.hh"
#include "helpers.hh"



void write_result(struct client_struct *thisclient, std::string command_id, std::string result) {
    write_to_client(thisclient, command_id + ':' + result);
}

void process_message(struct client_struct *thisclient, std::string_view message) {
    std::string result = "UNKOWN_COMMAND";
    std::vector<std::string> command, tempsplit;
    bool has_arg = false;

    // Debug
    std::cout << thisclient->get_identifier() << " > " << message << std::endl;

    // Split command
    std::clog << "Splitting command ID..." << std::flush;
    command = strsplit(message, ':', 1);
    std::clog << " Success!" << std::endl;
    if (command.size() < 2) {
        // Do nothing (error)
    } else {
        std::clog << "Splitting command itself.." << std::flush;
        tempsplit = strsplit(command[1], ' ', 1);
        if (tempsplit.size() > 1) {
            command[1] = tempsplit[0];
            command.push_back(tempsplit[1]);
            has_arg = true;
        }
        std::clog << " Success!" << std::endl;

        // Run it
        // Commands that require arguments
        if (!has_arg) {
            result = "MISSING_ARGS";
        }

        else if (tempsplit[1] == "AUTH") { // AUTH <user id> <password>
            // Split userid and password
            std::clog << "Splitting command arguments..." << std::flush;
            auto rawauth = strsplit(command[2], ' ', 1);
            std::clog << " Success!" << std::endl;
            // Check details
            if (rawauth.size() != 2) {
                result = "BAD_ARGS";
            } else if (!thisclient->authed) {
                // Check userid and password
                std::clog << "Authenticating \"" << rawauth[0] << "\" with password \"" << rawauth[1] << "\"..." << std::flush;
                user_struct *thisuser = check_auth(rawauth[0], rawauth[1]);
                if (thisuser != nullptr) {
                    thisclient->user = thisuser;
                    thisclient->authed = true;
                    result = "OK";
                }
            } else {
                result = "ALREADY_AUTHED";
            }
            std::clog << " Result: " << result << std::endl;
        }

    }

    // Write its result
    write_result(thisclient, command[0], result);

    // Debug
    std::cout << thisclient->get_identifier() << " < " << command[0] << ':' << result << std::endl;
}
