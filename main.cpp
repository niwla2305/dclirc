// Include C libraries.
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

// Then modernized C libraries.
#include <cstdio>
#include <cstdlib>

// C++ libraries...
#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <sstream>

// Other stuff
#include "settings.hh"
#include "exceptions.hh"
#include "structs.hh"
#include "clientproc.hh"
#include "environment.hh"
#include "users.hh"
#include "helpers.hh"

// And your own libraries, if any...
#include "clientproc.hh"


// Main code
int make_socket(uint16_t port) {
    // Iniiiiiiiiit.....
    int sock;
    struct sockaddr_in name{};

    // Create the socket
    sock = socket(PF_INET, SOCK_STREAM, 0);
    if (sock < 0) {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    // Give the socket a name
    name.sin_family = AF_INET;
    name.sin_port = htons(port);
    name.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(sock, reinterpret_cast<struct sockaddr *>(&name), sizeof(name)) < 0) {
        perror("bind");
        exit(EXIT_FAILURE);
    }

    return sock;
}



int main() {
    int sock = 0;
    fd_set active_fd_set{};
    fd_set read_fd_set{};
    int i = 0;
    struct sockaddr_in clientname{}; // Init every single POD, including structs.
    socklen_t size;

    // Load users
    load_users(users);

    // Create the socket and set it up to accept connections
    sock = make_socket(PORT);
    if (listen (sock, 1) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    // Initialize the set of active sockets
    FD_ZERO(&active_fd_set);
    FD_SET(sock, &active_fd_set);

    while (true) {
        // Block until input arrives on one or more active sockets
        read_fd_set = active_fd_set;
        if (select(FD_SETSIZE, &read_fd_set, nullptr, nullptr, nullptr) < 0) {
            perror("select");
            exit(EXIT_FAILURE);
        }

        // Service all the sockets with input pending
        for (i = 0; i < FD_SETSIZE; ++i) {
            if (FD_ISSET(i, &read_fd_set)) {
                if (i == sock) {
                    // Connection request on original socket
                    int newconn = accept(sock,
                                         reinterpret_cast<struct sockaddr *>(&clientname),
                                         &size);
                    size = sizeof(clientname);
                    if (newconn < 0) {
                        perror("accept");
                        exit(EXIT_FAILURE);
                    }
                    std::clog << "server: " << "connect from host "
                              << inet_ntoa(clientname.sin_addr)
                              << ":" << ntohs(clientname.sin_port)
                              << " (" << newconn << ')'
                              << std::endl;
                    FD_SET(newconn, &active_fd_set);
                    // Add client
                    struct client_struct thisclient;
                    thisclient.client_ip = inet_ntoa(clientname.sin_addr);
                    thisclient.conn_id = static_cast<unsigned int>(newconn);
                    clients[newconn] = thisclient;
                } else if (clients.find(i) != clients.end()) {
                    // Data arriving on an already-connected socket
                    try {
                        clients[i].recv_queue.push_back(read_from_client(&clients[i]));
                    } catch (nomessage&) {
                        // Nothing
                    } catch (client_error&) {
                        std::clog << "server: " << "disconnect of host "
                                  << inet_ntoa(clientname.sin_addr)
                                  << ":" << ntohs(clientname.sin_port)
                                  << " (" << i << ')'
                                  << std::endl;
                        clients.erase(i);
                        //close(i);
                    }
                }
            }
        }

        // Process queues (TODO: do that in another thread!!!)
        for(auto& client: clients) {
            auto *thisclient = &(client.second);

            if (!thisclient->recv_queue.empty()) {
                for (const std::string& message: thisclient->recv_queue)
                    process_message(thisclient, message);
                thisclient->recv_queue.clear();
            }
        }
    }
}
