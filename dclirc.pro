TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        clientproc.cpp \
        environment.cpp \
        helpers.cpp \
        main.cpp \
        users.cpp

HEADERS += \
    clientproc.hh \
    environment.hh \
    exceptions.hh \
    helpers.hh \
    settings.hh \
    structs.hh \
    users.hh
